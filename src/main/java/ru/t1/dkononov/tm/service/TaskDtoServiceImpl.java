package ru.t1.dkononov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.entity.model.Task;
import ru.t1.dkononov.tm.mapper.TaskMapper;
import ru.t1.dkononov.tm.repository.TaskRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskDtoServiceImpl implements TaskDtoService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<TaskDto> findAll() {
        return taskRepository.findAll()
                .stream()
                .map(TaskMapper::taskToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public TaskDto save(TaskDto task) {
        return TaskMapper.taskToDto(taskRepository.save(TaskMapper.dtoToTask(task)));
    }

    @Override
    public TaskDto save() {
        final TaskDto task = new TaskDto("New name" + LocalDateTime.now().toString());
        return TaskMapper.taskToDto(taskRepository.save(TaskMapper.dtoToTask(task)));
    }

    @Override
    public TaskDto findById(String id) {
        final Optional<Task> projectDtoOptional = taskRepository.findById(id);
        return projectDtoOptional.map(TaskMapper::taskToDto).orElse(null);
    }

    @Override
    public boolean exsistsById(String id) {
        return taskRepository.existsById(id);
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteById(TaskDto task) {
        taskRepository.delete(TaskMapper.dtoToTask(task));
    }

    @Override
    @Transactional
    public void deleteById(List<TaskDto> tasks) {
        List<Task> taskList = new ArrayList<>();
        taskList = tasks
                .stream()
                .map(TaskMapper::dtoToTask)
                .collect(Collectors.toList());
        taskRepository.deleteAll(taskList);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }


}
