package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.api.endpoint.TaskEndpoint;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.entity.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.dkononov.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskDtoService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDto save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    ) {
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/exsitsById/{id}")
    public boolean exsistsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.exsistsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    ) {
        taskService.deleteById(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDto> tasks
    ) {
        taskService.deleteById(tasks);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}
