package ru.t1.dkononov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.entity.model.Project;
import ru.t1.dkononov.tm.mapper.ProjectMapper;
import ru.t1.dkononov.tm.repository.ProjectRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectDtoServiceImpl implements ProjectDtoService {

    @Autowired
    private ProjectRepository projectRepository;


    @Override
    public List<ProjectDto> findAll() {
        return projectRepository.findAll()
                .stream()
                .map(ProjectMapper::projectToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProjectDto save(final ProjectDto project) {
        return ProjectMapper.projectToDto(projectRepository.save(ProjectMapper.dtoToProject(project)));
    }

    @Override
    public ProjectDto save() {
        final ProjectDto project = new ProjectDto("New name " + LocalDateTime.now().toString());
        return ProjectMapper.projectToDto(projectRepository.save(ProjectMapper.dtoToProject(project)));
    }

    @Override
    public ProjectDto findById(final String id) {
        final Optional<Project> projectDtoOptional = projectRepository.findById(id);
        return projectDtoOptional.map(ProjectMapper::projectToDto).orElse(null);
    }

    @Override
    public boolean exsistsById(final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Override
    public void deleteById(final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    public void delete(final ProjectDto project) {
        projectRepository.delete(ProjectMapper.dtoToProject(project));
    }

    @Override
    public void deleteAll(final List<ProjectDto> projectsDto) {
        List<Project> projects = new ArrayList<>();
        projects = projectsDto
                .stream()
                .map(ProjectMapper::dtoToProject)
                .collect(Collectors.toList());
        projectRepository.deleteAll(projects);
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

}
