package ru.t1.dkononov.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @Value("#{environment['server.host']}")
    public String serverHost;

    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private String sessionTimeout;

    @Value("#{environment['database.username']}")
    private String databaseUser;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2auto;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLvlCache;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheRegionFactory;

    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseProviderConfig;

    @Value("#{environment['token.init']}")
    private String tokenInit;

    public final Properties properties = new Properties();

    public PropertyService() {
    }

}
