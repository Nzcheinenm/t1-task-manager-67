package ru.t1.dkononov.tm.api.service;

public interface IPropertyService {
    String getDatabaseDriver();

    String getDatabaseUrl();

    String getDatabaseUser();

    String getDatabasePassword();

    String getDatabaseDialect();

    String getDatabaseHbm2auto();

    String getDatabaseShowSql();

    String getDatabaseUseSecondLvlCache();

    String getDatabaseUseQueryCache();

    String getDatabaseUseMinimalPuts();

    String getDatabaseCacheRegionPrefix();

    String getDatabaseCacheRegionFactory();

    String getDatabaseProviderConfig();
}
