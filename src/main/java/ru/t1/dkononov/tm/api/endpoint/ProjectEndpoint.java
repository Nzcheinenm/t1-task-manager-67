package ru.t1.dkononov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface ProjectEndpoint {
    @WebMethod
    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @WebMethod
    @PostMapping("/save")
    ProjectDto save(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/exsitsById/{id}")
    boolean exsistsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody List<ProjectDto> projects
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();
}
